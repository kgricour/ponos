#!/usr/bin/python3

from oauth2client.service_account import ServiceAccountCredentials
import gspread as gs
from gspread_formatting import *

import os
from datetime import datetime
import sys
from hashlib import sha256
import random
from colorsys import hls_to_rgb

SCOPE = ["https://spreadsheets.google.com/feeds",
         "https://www.googleapis.com/auth/drive"]

SPREADSHEET_KEY = "1iByJ89eHPaY2QHMdKK8wvzpaiJWIqI7Zjgzg9zSNBSs"
FTIME = "%d/%m/%Y %H:%M:%S"

def hue_from_str(str):
    random.seed(int(sha256(str.encode('utf-8')).hexdigest(), 16))
    return hls_to_rgb(random.random(), 0.7, 0.7)

def xy_to_idx(x, y):
    return (y - 1) * 2 + (x - 1)

def reformate(logssheet):
    cells_range = "A1:B{0}".format(logssheet.row_count)
    print("Getting cells ({})".format(cells_range))
    cells = logssheet.range(cells_range)
    for i in range(1, logssheet.row_count + 1):
        if not (cells[xy_to_idx(1, i)].value == "start" or cells[xy_to_idx(1, i)].value == "end"):
            break

        date = str(datetime.strptime(cells[xy_to_idx(2, i)].value, FTIME).date())
        fmt_range = "A{0}:C{0}".format(i)
        print("Formatting cells ({})".format(fmt_range))
        (r, g, b) = hue_from_str(date)
        fmt = CellFormat(backgroundColor=Color(r, g, b))
        format_cell_range(logssheet, fmt_range, fmt)

def main():
    credentials_path = os.path.join(os.getenv("HOME"), ".config/ponos/credentials.json")
    print("Loading credentials from file: \"%s\"" % credentials_path)
    credentials = ServiceAccountCredentials.from_json_keyfile_name(credentials_path, SCOPE)

    print("Connecting")
    gc = gs.authorize(credentials)

    print("Opening spreadsheet with key: \"%s\"" % SPREADSHEET_KEY)
    spreadsheet = gc.open_by_key(SPREADSHEET_KEY)
    logssheet = spreadsheet.worksheet("Logs")

    if len(sys.argv) > 1 and sys.argv[1] == "reformate":
        reformate(logssheet)
        exit()

    print("Getting cells (A1:C1)")
    cells = logssheet.range("A1:C1")

    print("Processing data")
    current_time = datetime.now()
    new_row = ["ERROR"]
    if cells[0].value == "start":
        session_hours_formula = "=(INDIRECT(ADDRESS(ROW();COLUMN()-1; ;FALSE); FALSE) - INDIRECT(ADDRESS(ROW()+1;COLUMN()-1; ;FALSE); FALSE))"
        new_row = ["end", current_time.strftime(FTIME), session_hours_formula]

    elif cells[0].value == "end" or cells[0].value == "":
        new_row = ["start", current_time.strftime(FTIME)]

    print("Inserting \"%s ; %s\"" % (new_row[0], new_row[1]))
    logssheet.insert_row(new_row, value_input_option="USER_ENTERED")

    print("Formatting inserted row")
    (r, g, b) = hue_from_str(str(current_time.date()))
    fmt = CellFormat(backgroundColor=Color(r, g, b))
    format_cell_range(logssheet, "A1:C1", fmt)

if __name__ == "__main__":
    main()
